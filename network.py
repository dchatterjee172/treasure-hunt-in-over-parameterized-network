import numpy as np
from collections import namedtuple
import jax.numpy as jnp
from jax import grad, jit, random
from functools import partial
from terminalplot import plot
import gc


class NN(namedtuple("NN", ("w1", "b1", "w2", "b2"))):
    @staticmethod
    def get(input_size: int, hidden_size: int, output_size: int, seed: int):
        key = random.PRNGKey(seed)
        w1 = random.normal(key, shape=(input_size, hidden_size)) * jnp.sqrt(
            2 / input_size
        )
        w2 = random.normal(key, shape=(hidden_size, output_size)) * jnp.sqrt(
            2 / hidden_size
        )
        b1 = jnp.zeros(shape=(hidden_size,))
        b2 = jnp.zeros(shape=(output_size,))
        return NN(w1=w1, b1=b1, w2=w2, b2=b2)

    @staticmethod
    def set(w1, b1, w2, b2):
        return NN(w1=w1, b1=b1, w2=w2, b2=b2)

    def __add__(self, other):
        return NN(
            w1=self.w1 + other.w1,
            b1=self.b1 + other.b1,
            w2=self.w2 + other.w2,
            b2=self.b2 + other.b2,
        )

    def __sub__(self, other):
        return NN(
            w1=self.w1 - other.w1,
            b1=self.b1 - other.b1,
            w2=self.w2 - other.w2,
            b2=self.b2 - other.b2,
        )

    def __mul__(self, scalar: float):
        return NN(
            w1=self.w1 * scalar,
            b1=self.b1 * scalar,
            w2=self.w2 * scalar,
            b2=self.b2 * scalar,
        )

    __rmul__ = __mul__

    def __repr__(self):
        return f"{' -> '.join(str(i) for i in self.shape)}"

    __str__ = __repr__

    @property
    def shape(self):
        return (self.w1.shape[0], self.w1.shape[1], self.w2.shape[1])


def drop_weights_nn(nn: NN, p: float, seed: int):
    key = random.PRNGKey(seed)
    w1 = nn.w1 * random.bernoulli(key=key, p=p, shape=nn.w1.shape)
    w2 = nn.w2 * random.bernoulli(key=key, p=p, shape=nn.w2.shape)
    return NN(w1=w1, b1=nn.b1, w2=w2, b2=nn.b2)


def forward(nn: NN, data: np.ndarray):
    o1 = jnp.matmul(data, nn.w1) + nn.b1
    a1 = jnp.tanh(o1 * 0.6667) * 1.714
    o2 = jnp.matmul(a1, nn.w2) + nn.b2
    a2 = o2 - jnp.log(jnp.exp(o2).sum(axis=1, keepdims=True))
    return a2


def loss(nn: NN, data: np.ndarray, labels: np.ndarray):
    pred = forward(nn, data)
    loss = jnp.mean(-(labels * pred).sum(1))
    return loss


@jit
def accuracy(nn: NN, data: np.ndarray, labels: np.ndarray):
    pred = jnp.argmax(forward(nn, data), 1)
    result = labels == pred
    result = result.astype(jnp.float32)
    return result.mean()


g_loss = grad(loss)


def train(
    nn, training_image, training_label, test_image, test_label, learning_rate, epoch
):
    labels_one_hot = np.eye(training_label.max() + 1)[training_label.reshape(-1)]
    grad_loss = partial(g_loss, data=training_image, labels=labels_one_hot)
    momentum = grad_loss(nn)
    acc = []

    @jit
    def gd(nn: NN, momentum: NN, learning_rate: float):
        g = grad_loss(nn)
        momentum = momentum * 0.9 + g * 0.1
        return nn - momentum * learning_rate, momentum

    for i in range(epoch):
        nn, momentum = gd(nn, momentum, learning_rate)
        if i % 10 == 0:
            acc.append(accuracy(nn, test_image, test_label))
            plot(range(len(acc)), acc)
            gc.collect()
    return nn


# ###########################################################


class Score(namedtuple("Score", ("s1", "s2", "top_k"))):
    ep = 0.00001

    @staticmethod
    def get(
        input_size: int, hidden_size: int, output_size: int, top_k: float, seed: int
    ):
        key = random.PRNGKey(seed)
        s1 = random.uniform(key, minval=0.1, maxval=1, shape=(input_size, hidden_size))
        s2 = random.uniform(key, minval=0.1, maxval=1, shape=(hidden_size, output_size))
        return Score(s1=s1, s2=s2, top_k=top_k)

    def __add__(self, other):
        s1 = self.s1 + other.s1
        s2 = self.s2 + other.s2
        return Score(s1=s1, s2=s2, top_k=self.top_k)

    def __sub__(self, other):
        s1 = self.s1 - other.s1
        s2 = self.s2 - other.s2
        return Score(s1=s1, s2=s2, top_k=self.top_k)

    def __mul__(self, scalar: float):
        s1 = self.s1 * scalar
        s2 = self.s2 * scalar
        return Score(s1=s1, s2=s2, top_k=self.top_k)

    def update(self, neg_delta):
        s1 = self.s1 - neg_delta.s1
        s1 = jnp.where(s1 <= 0, jnp.zeros_like(s1) + Score.ep, s1)
        s2 = self.s2 - neg_delta.s2
        s2 = jnp.where(s2 <= 0, jnp.zeros_like(s2) + Score.ep, s2)
        return Score(s1=s1, s2=s2, top_k=self.top_k)

    __rmul__ = __mul__

    def __repr__(self):
        return f"{self.s1.shape[0]} -> {self.s1.shape[1]} -> {self.s2.shape[1]} \
                top {self.top_k}%"


def get_top_k(score: np.ndarray, top_k):
    return jnp.where(
        score >= score.flatten().sort()[-(int(top_k / 100 * score.size))],
        jnp.ones_like(score),
        jnp.zeros_like(score),
    )


@partial(jit, static_argnums=(3,))
def forward_mask_infer(nn: NN, score: Score, data: np.ndarray, top_k):
    o1 = jnp.matmul(data, nn.w1 * get_top_k(score.s1, top_k)) + nn.b1
    a1 = jnp.tanh(o1 * 0.6667) * 1.714
    o2 = jnp.matmul(a1, nn.w2 * get_top_k(score.s2, top_k)) + nn.b2
    a2 = o2 - jnp.log(jnp.exp(o2).sum(axis=1, keepdims=True))
    return a2


def forward_mask_train(nn: NN, score: Score, data: np.ndarray):
    o1 = jnp.matmul(data, nn.w1 * score.s1) + nn.b1
    a1 = jnp.tanh(o1 * 0.6667) * 1.714
    o2 = jnp.matmul(a1, nn.w2 * score.s2) + nn.b2
    a2 = o2 - jnp.log(jnp.exp(o2).sum(axis=1, keepdims=True))
    return a2


def loss_mask(score: Score, nn: NN, data: np.ndarray, labels: np.ndarray):
    pred = forward_mask_train(nn, score, data)
    loss = jnp.mean(-(labels * pred).sum(1))
    return loss


g_loss_mask = grad(loss_mask)


@partial(jit, static_argnums=(4,))
def accuracy_mask(
    nn: NN, score: Score, data: np.ndarray, labels: np.ndarray, top_k: float
):
    pred = jnp.argmax(forward_mask_infer(nn, score, data, top_k), 1)
    result = labels == pred
    result = result.astype(jnp.float32)
    return result.mean()


def train_score(
    nn,
    score,
    training_image,
    training_label,
    test_image,
    test_label,
    learning_rate,
    epoch,
):
    labels_one_hot = np.eye(training_label.max() + 1)[training_label.reshape(-1)]
    grad_loss = partial(g_loss_mask, data=training_image, labels=labels_one_hot, nn=nn)
    momentum = grad_loss(score)
    acc = []

    @jit
    def gd(score: Score, momentum: Score, learning_rate: float):
        g = grad_loss(score)
        momentum = momentum * 0.0 + g * 1.0
        return score.update(momentum * learning_rate), momentum

    for i in range(epoch):
        score, momentum = gd(score, momentum, learning_rate)
        if i % 10 == 0:
            acc.append(accuracy_mask(nn, score, test_image, test_label, score.top_k))
            plot(range(len(acc)), acc)
            gc.collect()
    return score
