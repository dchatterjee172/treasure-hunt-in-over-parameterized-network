from get_mnist import load
from network import train, Score, train_score, NN, get_top_k
from numpy.linalg import matrix_rank
from sklearn.datasets import make_classification
from sklearn.model_selection import train_test_split


def run(training_image, training_label, test_image, test_label, nn, score, name):
    input(f"{name} {nn}")
    new_nn = train(nn, training_image, training_label, test_image, test_label, 0.1, 200)
    new_hidden_transform_mat = new_nn.w1.T
    print(matrix_rank(new_hidden_transform_mat))
    del new_nn, new_hidden_transform_mat
    print()
    input(f"{name} {score}")
    new_score = train_score(
        nn, score, training_image, training_label, test_image, test_label, 10, 200
    )
    s1 = get_top_k(new_score.s1, new_score.top_k)
    new_hidden_transform_mat = (nn.w1 * s1).T
    print(matrix_rank(new_hidden_transform_mat))
    del s1, new_hidden_transform_mat, new_score


if __name__ == "__main__":
    seed_1 = 213
    seed_2 = 121
    nn = NN.get(784, 200, 10, seed_1)
    score = Score.get(*nn.shape, 50.0, seed_2)
    for cat in ("digit", "fashion"):
        training_image, training_label, test_image, test_label = load(cat)
        run(training_image, training_label, test_image, test_label, nn, score, cat)
    for i in range(2):
        X, y = make_classification(
            n_samples=90000,
            n_features=784,
            n_informative=400,
            n_redundant=100,
            random_state=i,
            n_classes=10,
            class_sep=4,
        )
        training_image, test_image, training_label, test_label = train_test_split(
            X, y, test_size=0.33, random_state=i, stratify=y
        )
        run(
            training_image,
            training_label,
            test_image,
            test_label,
            nn,
            score,
            f"synthetic_{i + 1}",
        )
