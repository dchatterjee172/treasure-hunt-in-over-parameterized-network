import numpy as np
from urllib import request
import gzip
import shelve
import tempfile
import os.path as path

filename = (
    ("training_images", "train-images-idx3-ubyte.gz"),
    ("test_images", "t10k-images-idx3-ubyte.gz"),
    ("training_labels", "train-labels-idx1-ubyte.gz"),
    ("test_labels", "t10k-labels-idx1-ubyte.gz"),
)


def get_mnist():
    base_urls = {
        "digit": "http://yann.lecun.com/exdb/mnist/",
        "fashion": "http://fashion-mnist.s3-website.eu-central-1.amazonaws.com/",
    }
    data = shelve.open("data")
    with tempfile.TemporaryDirectory() as tmpdirname:
        for cat, base_url in base_urls.items():
            mnist = {}
            print(cat)
            for name in filename:
                print(f"Downloading {name[1]}")
                request.urlretrieve(base_url + name[1], path.join(tmpdirname, name[1]))
            for name in filename[:2]:
                with gzip.open(path.join(tmpdirname, name[1]), "rb") as f:
                    mnist[name[0]] = np.frombuffer(
                        f.read(), np.uint8, offset=16
                    ).reshape(-1, 28 * 28)
            for name in filename[-2:]:
                with gzip.open(path.join(tmpdirname, name[1]), "rb") as f:
                    mnist[name[0]] = np.frombuffer(f.read(), np.uint8, offset=8)
            data[cat] = mnist
    data.close()


def load(cat):
    data = shelve.open("data")
    mnist = data[cat]
    return (
        (mnist["training_images"].astype(np.float32) - 127.5) / 127.5,
        mnist["training_labels"],
        (mnist["test_images"].astype(np.float32) - 127.5) / 127.5,
        mnist["test_labels"],
    )


if __name__ == "__main__":
    get_mnist()
